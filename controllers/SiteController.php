<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\lleva;
use app\models\puerto;
use yii\data\SqlDataProvider;


class SiteController extends Controller
{
    
  
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
     public function actionConsulta1(){
        $dataProvider = new SqlDataProvider(['sql'=>'SELECT DISTINCT COUNT(dorsal) as total FROM ciclista'

        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>" Número de ciclistas que hay",
            "sql"=>"SELECT DISTINCT COUNT(dorsal) as total FROM ciclista",
        ]);
     }

    public function actionConsulta1a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()->select("COUNT(dorsal) as total")->distinct(),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 1 con Action Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT DISTINCT COUNT(dorsal) as total FROM ciclista",
        ]);
}
    
      
  
    public function actionConsulta2(){
        
        $dataProvider = new SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT COUNT(dorsal) as sumabanesto FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['sumabanesto'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>'SELECT DISTINCT COUNT(dorsal)as sumabanesto FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
    }
      
    public function actionConsulta2a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()
                ->select("COUNT(dorsal) as sumabanesto")
                ->distinct()
                ->where('nomequipo = "banesto"'),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['sumabanesto'],
            "titulo"=>"Consulta 2 con Action Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(dorsal) as sumabanesto FROM ciclista WHERE nomequipo = 'banesto'",
        ]);
    }
    
       
 
    public function actionConsulta3(){
        
        $dataProvider = new SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT AVG (edad) as mediaedad FROM ciclista',
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediaedad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT DISTINCT AVG (edad) as mediaedad FROM ciclista",
        ]);
    }

    public function actionConsulta3a(){
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()
                   ->select("AVG(edad) as mediaedad")
                   ->distinct(),
                   
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediaedad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT DISTINCT AVG (edad) as mediaedad FROM ciclista",
        ]);
    }
 
     public function actionConsulta4(){
        
        $dataProvider = new SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT AVG (edad) as mediabanesto FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediabanesto'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los ciclistas del equipo Banesto",
            "sql"=>'"SELECT DISTINCT AVG (edad) as mediabanesto FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
    }

    public function actionConsulta4a(){
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()
                   ->select("AVG(edad) as mediabanesto")
                   ->distinct()
                   ->where("nomequipo = 'Banesto'"),
                   
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediabanesto'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los ciclistas del equipo Banesto",
            "sql"=>"SELECT DISTINCT AVG(edad) as mediabanesto FROM ciclista",
        ]);
    }
    

    public function actionConsulta5(){
        
        $dataProvider = new SqlDataProvider([
            
            'sql'=>'SELECT AVG (edad) as mediaequipos, nomequipo FROM  ciclista GROUP BY nomequipo',
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediaequipos','nomequipo'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG (edad) as mediaequipos FROM ciclista Group BY nomequipo",
        ]);
    }

    public function actionConsulta5a(){
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("AVG(edad) as mediaequipos, nomequipo")
                ->groupBy("nomequipo"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediaequipos','nomequipo'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG (edad) as mediaequipos, nomequipo FROM  ciclista GROUP BY nomequipo",
        ]);
    }
   
    public function actionConsulta6(){
        
        $dataProvider = new SqlDataProvider([
            
            'sql'=>'SELECT COUNT(dorsal) as ciclistasequipo, nomequipo FROM ciclista GROUP BY nomequipo'
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','ciclistasequipo'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>" El número de ciclistas por equipo",
            "sql"=>"SELECT COUNT(dorsal) as ciclistasequipo,nomequipo FROM ciclista GROUP BY nomequipo",
        ]);
    }

    public function actionConsulta6a(){
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("COUNT(dorsal) as ciclistasequipo,nomequipo")
                ->GROUPBY("nomequipo"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','ciclistasequipo'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>" El número de ciclistas por equipo",
            "sql"=>"SELECT COUNT(dorsal) as ciclistasequipo,nomequipo FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    
    public function actionConsulta7(){
        
        $dataProvider = new SqlDataProvider([
            
            'sql'=>'SELECT COUNT(nompuerto) AS puertostotal FROM puerto'
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['puertostotal'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(nompuerto) AS puertostotal",
        ]);
    }
        
    public function actionConsulta7a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("COUNT(nompuerto) as puertostotal")
                                     ->distinct(),
        ]);
         return $this->render("resultado", [
            "resultados"=>$dataProvider, 
            "campos"=>['puertostotal'],
            "titulo"=> "Consulta 7 con ActiveDataProvider ",
            "enunciado" => "El número total de puertos",
            "sql" =>'SELECT COUNT(nompuerto) AS puertostotal',
        ]);
   }
   
 
   public function actionConsulta8(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT COUNT(nompuerto) AS puertostotal FROM puerto WHERE altura > 1500'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['puertostotal'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(nompuerto) as puertostotal FROM puerto WHERE altura > 1500",
                ]);
    }
   
    public function actionConsulta8a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Puerto::find()->select("COUNT(nompuerto) AS puertostotal")
                                 ->distinct()
                                 ->where("altura > 1500"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['puertostotal'],
            "titulo"=>"Consulta 8 con Action Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(nompuerto) as puertostotal FROM puerto WHERE altura > 1500",
        ]);
    }
   
  public function actionConsulta9(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(dorsal)>4'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT COUNT(dorsal),nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(dorsal)>4",
                ]);
    }
   
    public function actionConsulta9a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()->select("nomequipo")
                                   ->groupBy("nomequipo")
                                   ->having("COUNT(dorsal)>4"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Action Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY  nomequipo HAVING COUNT(dorsal)>4",
        ]);
    }
     
    public function actionConsulta10(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo FROM ciclista GROUP BY("nomequipo") Having(count(dorsal>4) AND "edad" BETWEEN 28 AND 32)',
        ]);

        return $this->render("resultado", [
            "resultados"=>$dataProvider, 
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 10 con DAO ",
            "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql" =>'SELECT nomequipo FROM ciclista GROUP BY("nomequipo") Having(count(dorsal>4) AND "edad" BETWEEN 28 AND 32)',
        ]);
    }
   
       public function actionConsulta10a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo")
                ->GROUPBY("nomequipo")
                ->HAVING("COUNT(dorsal)>4 AND 'edad' BETWEEN 28 AND 32"),
        ]);
         return $this->render("resultado", [
            "resultados"=>$dataProvider, 
            "campos"=>['nomequipo'],
            "titulo"=> "Consulta 10 con ActiveDataProvider ",
            "enunciado" =>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql" => 'SELECT nomequipo FROM ciclista GROUP BY("nomequipo") Having(count(dorsal>4) AND "edad" BETWEEN 28 AND 32)',
        ]);
    }
   
    public function actionConsulta11(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal,COUNT(dorsal) as etapasganadas FROM Etapa GROUP BY dorsal'
        ]);

        return $this->render("resultado", [
            "resultados"=>$dataProvider, 
            "campos"=>['dorsal','etapasganadas'],
            "titulo"=> "Consulta 11 con DAO ",
            "enunciado" =>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql" =>'SELECT dorsal, COUNT(dorsal) as etapasganadas FROM Etapa GROUP BY dorsal',
        ]);
    }
   
      public function actionConsulta11a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()->select("dorsal, COUNT(dorsal) AS etapaganada")
                ->GROUPBY("dorsal"),
        ]);
         return $this->render("resultado", [
            "resultados"=>$dataProvider, 
            "campos"=>['dorsal','etapasganadas'],
            "titulo"=> "Consulta 11 con ActiveDataProvider ",
            "enunciado" =>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql" =>'SELECT dorsal, COUNT(dorsal) as etapasganadas FROM Etapa GROUP BY dorsal',
        ]);
    }
    
    public function actionConsulta12(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT dorsal  FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal  FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1",
                ]);
    }
   
    public function actionConsulta12a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Etapa::find()->select("dorsal")->groupBy("dorsal")->having("COUNT(dorsal)>1"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con Action Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal  FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1",
        ]);
    }


}
